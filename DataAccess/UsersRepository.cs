﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class UsersRepository:ConnectionClass
    {
        public IQueryable<User> GetUsers()
        {
            return Entity.Users;
        }


        public IQueryable<User> GetUsers(string keyword)
        {
            return Entity.Users.Where(u => u.Email.Contains(keyword)
            || u.Name.Contains(keyword) || u.Surname.Contains(keyword));


        }

        public User GetUser(string username)
        {
            return Entity.Users.SingleOrDefault(u => u.Username == username);
        }


        public void AddUser(User u)
        {
            Entity.Users.Add(u);
            Entity.SaveChanges();
        }

        public void Delete(string username)
        {
            var user = GetUser(username);
            if (user != null)
                Entity.Users.Remove(user);

            Entity.SaveChanges();
        }

        public void Update(User u)
        {
            var original = GetUser(u.Username);

            original.Name = u.Name;
            original.Surname = u.Surname;
            original.Location_Fk = u.Location_Fk;
            original.Email = u.Email;

            Entity.SaveChanges();
        }
    }
}
