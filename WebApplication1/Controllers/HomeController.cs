﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View(new CurrencyConversionModel());
        }

        [HttpPost]
        public ActionResult Index(string from, string to, string v)
        {
            net.kowabunga.currencyconverter.Converter c = new net.kowabunga.currencyconverter.Converter();
            decimal rate = c.GetConversionRate(from, to, new DateTime(2019, 2, 11));
            decimal result = Convert.ToDecimal(v) * rate;


            ViewBag.Result = result;

            return View(new CurrencyConversionModel());
        }
    }
}