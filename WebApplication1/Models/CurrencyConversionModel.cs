﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class CurrencyConversionModel
    {
        public string [] CurrencyList { get; set; }

        public CurrencyConversionModel()
        {
            net.kowabunga.currencyconverter.Converter c = new net.kowabunga.currencyconverter.Converter();
            CurrencyList = c.GetCurrencies();
        }
    }
}