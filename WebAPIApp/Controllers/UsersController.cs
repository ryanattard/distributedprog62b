﻿using Common;
using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPIApp.Controllers
{
    public class UsersController : ApiController
    {

        #region READ operations
        [Route("api/Users")]
        public IHttpActionResult GetUsers()
        {
            UsersRepository ur = new UsersRepository();

            var myCustomList = from u in ur.GetUsers()
                               select new
                               {
                                   Username = u.Username,
                                   Name = u.Name,
                                   Surname = u.Surname,
                                   Location = u.Location.Name,
                                   Location_Fk = u.Location_Fk,
                                   DateOfBirth = u.DateOfBirth
                               };

            return Ok(myCustomList);
        }

        [Route("api/Users/Search/{keyword}")]
        public IHttpActionResult GetUsers(string keyword)
        {
            UsersRepository ur = new UsersRepository();
            var list = ur.GetUsers(keyword);
            return Ok(list);

        }

        [Route("api/Users/{username}")]
        public IHttpActionResult GetUser(string username)
        {
            UsersRepository ur = new UsersRepository();
            var user = ur.GetUser(username);
            return Ok(user);

        }
        #endregion

        #region CREATE operations

        [HttpPost, Route("api/Users/create")]
        public IHttpActionResult AddUser([FromBody] User u)
        {
            try
            {
                UsersRepository ur = new UsersRepository();
                //implement validation
                //if validation is not ok return BadRequest();

                if (u.Name == "") return BadRequest("Name is empty");


                ur.AddUser(u);
                return Ok();
            }
            catch(Exception ex)
            {
                //log the error
                
                return InternalServerError(new Exception("User failed to be registered"));
            }

        }


        #endregion

        #region DELETE operations

        [HttpDelete]
        [Route("api/Users/{username}")]
        public IHttpActionResult DeleteUser(string username)
        {
            try
            {
                UsersRepository ur = new UsersRepository();
                if(ur.GetUser(username) == null)
                {
                    return BadRequest("username does not exist");
                }
                else
                ur.Delete(username);
                return Ok();

            }
            catch (Exception ex)
            {

                return InternalServerError();
            }
        }

        #endregion

        #region UPDATE operations
        #endregion

    }
}