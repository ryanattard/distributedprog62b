﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WebAPIApp.Models;
using System.Web.Configuration;
using System.Text;

namespace WebAPIApp.Controllers
{
    public class NewsController : ApiController
    {
        // GET api/<controller>
        [Route("api/News")]
        public async Task<IHttpActionResult> Get()
        {
            string url = "https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=9b970b3d567a48b3a8da388ba7e94193";
            HttpClient client = new HttpClient();
            string results = await  client.GetStringAsync(url);

            //deserialization into json object.
            //- Method 1
             NewsArticles articles = JsonConvert.DeserializeObject<NewsArticles>(results);

            //- Method 2
            dynamic myDynamicArticles = JsonConvert.DeserializeObject(results);
            NewsArticles mydynamicList = new NewsArticles();
            mydynamicList.articles = new List<News>();
            foreach(var a in myDynamicArticles.articles)
            {
                mydynamicList.articles.Add(new News()
                {
                    author = a.author,
                    publishedAt = a.publishedAt,
                    description = a.description

                });
            }

            //do some processing with the articles received
            return Ok(articles);
        }

        [Route("api/News/create")] [HttpPost]
        public async Task<IHttpActionResult> CreateUser([FromBody] Common.User u)
        {
            string baseAddress = WebConfigurationManager.AppSettings["baseAddress"];

            string url = baseAddress+ "/api/Users/create";
            HttpClient client = new HttpClient();

            string uAsJson = JsonConvert.SerializeObject(u);

            HttpContent content = new StringContent(uAsJson, Encoding.UTF8, "application/json");
  
            var result = await client.PostAsync(url, content);

            if (result.IsSuccessStatusCode)
                return Ok();
            else
            {
                return InternalServerError();
            }
        }



    }
}