﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPIApp.Controllers
{
    public class LocationsController : ApiController
    {

        [Route("api/locations")]
        public IHttpActionResult GetLocations()
        {
            return Ok();
        }

        [Route("api/locations/users")]
        public IHttpActionResult GetLocationsAndUsersLivingInLocation()
        {
            var list = from l in new LocationsRepository().GetLocations()
                       select new
                       {
                           Id = l.Id,
                           Name = l.Name,
                           Users = from u in l.Users
                                   select new
                                   {
                                       Username = u.Username,
                                       Name = u.Name,
                                       Surname = u.Surname
                                   }
                       };

            return Ok(list);

        }
    }
}