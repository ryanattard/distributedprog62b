﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPIApp.Models
{
    public class News
    {
        public DateTime publishedAt { get; set; }
        public string author { get; set; }
        public string description { get; set; }
    }

    public class NewsArticles
    {
        public int totalResults { get; set; }
        public List<News> articles { get; set; }
    }
}